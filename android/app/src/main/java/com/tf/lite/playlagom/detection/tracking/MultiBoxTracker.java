/* Copyright 2019 The TensorFlow Authors. All Rights Reserved.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
==============================================================================*/

package com.tf.lite.playlagom.detection.tracking;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Paint.Cap;
import android.graphics.Paint.Join;
import android.graphics.Paint.Style;
import android.graphics.RectF;
import android.media.MediaPlayer;
import android.text.TextUtils;
import android.util.Log;
import android.util.Pair;
import android.util.TypedValue;

import com.tf.lite.playlagom.MainActivity;
import com.tf.lite.playlagom.detection.env.BorderedText;
import com.tf.lite.playlagom.detection.env.ImageUtils;
import com.tf.lite.playlagom.detection.env.Logger;
import com.tf.lite.playlagom.detection.tflite.Classifier.Recognition;

import org.tensorflow.lite.examples.classification.R;

import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import static com.tf.lite.playlagom.MainActivity.mp;

/** A tracker that handles non-max suppression and matches existing objects to new detections. */
public class MultiBoxTracker {
  private static final float TEXT_SIZE_DIP = 18;
  private static final float MIN_SIZE = 16.0f;
  private static final int[] COLORS = {
    Color.BLUE,
    Color.RED,
    Color.GREEN,
    Color.YELLOW,
    Color.CYAN,
    Color.MAGENTA,
    Color.WHITE,
    Color.parseColor("#55FF55"),
    Color.parseColor("#FFA500"),
    Color.parseColor("#FF8888"),
    Color.parseColor("#AAAAFF"),
    Color.parseColor("#FFFFAA"),
    Color.parseColor("#55AAAA"),
    Color.parseColor("#AA33AA"),
    Color.parseColor("#0D0068")
  };
  final List<Pair<Float, RectF>> screenRects = new LinkedList<Pair<Float, RectF>>();
  private final Logger logger = new Logger();
  private final Queue<Integer> availableColors = new LinkedList<Integer>();
  private final List<TrackedRecognition> trackedObjects = new LinkedList<TrackedRecognition>();
  private final Paint boxPaint = new Paint();
  private final float textSizePx;
  private final BorderedText borderedText;
  private Matrix frameToCanvasMatrix;
  private int frameWidth;
  private int frameHeight;
  private int sensorOrientation;

  public MultiBoxTracker(final Context context) {
    for (final int color : COLORS) {
      availableColors.add(color);
    }

    boxPaint.setColor(Color.RED);
    boxPaint.setStyle(Style.STROKE);
    boxPaint.setStrokeWidth(10.0f);
    boxPaint.setStrokeCap(Cap.ROUND);
    boxPaint.setStrokeJoin(Join.ROUND);
    boxPaint.setStrokeMiter(100);

    textSizePx =
        TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_DIP, TEXT_SIZE_DIP, context.getResources().getDisplayMetrics());
    borderedText = new BorderedText(textSizePx);
  }

  public synchronized void setFrameConfiguration(
      final int width, final int height, final int sensorOrientation) {
    frameWidth = width;
    frameHeight = height;
    this.sensorOrientation = sensorOrientation;
  }

  public synchronized void drawDebug(final Canvas canvas) {
    final Paint textPaint = new Paint();
    textPaint.setColor(Color.WHITE);
    textPaint.setTextSize(60.0f);

    final Paint boxPaint = new Paint();
    boxPaint.setColor(Color.RED);
    boxPaint.setAlpha(200);
    boxPaint.setStyle(Style.STROKE);

    for (final Pair<Float, RectF> detection : screenRects) {
      final RectF rect = detection.second;
      canvas.drawRect(rect, boxPaint);
      canvas.drawText("" + detection.first, rect.left, rect.top, textPaint);
      borderedText.drawText(canvas, rect.centerX(), rect.centerY(), "" + detection.first);
    }
  }

  public synchronized void trackResults(final List<Recognition> results, final long timestamp) {
    logger.i("Processing %d results from %d", results.size(), timestamp);
    processResults(results);
  }

  private Matrix getFrameToCanvasMatrix() {
    return frameToCanvasMatrix;
  }

  public synchronized void draw(final Canvas canvas) {
    final boolean rotated = sensorOrientation % 180 == 90;
    final float multiplier =
        Math.min(
            canvas.getHeight() / (float) (rotated ? frameWidth : frameHeight),
            canvas.getWidth() / (float) (rotated ? frameHeight : frameWidth));
    frameToCanvasMatrix =
        ImageUtils.getTransformationMatrix(
            frameWidth,
            frameHeight,
            (int) (multiplier * (rotated ? frameHeight : frameWidth)),
            (int) (multiplier * (rotated ? frameWidth : frameHeight)),
            sensorOrientation,
            false);
    for (final TrackedRecognition recognition : trackedObjects) {
      final RectF trackedPos = new RectF(recognition.location);

      getFrameToCanvasMatrix().mapRect(trackedPos);
      boxPaint.setColor(recognition.color);

      float cornerSize = Math.min(trackedPos.width(), trackedPos.height()) / 8.0f;
      canvas.drawRoundRect(trackedPos, cornerSize, cornerSize, boxPaint);

      final String labelString =
          !TextUtils.isEmpty(recognition.title)
              ? String.format("%s %.2f", recognition.title, (100 * recognition.detectionConfidence))
              : String.format("%.2f", (100 * recognition.detectionConfidence));
      //            borderedText.drawText(canvas, trackedPos.left + cornerSize, trackedPos.top,
      // labelString);
      borderedText.drawText(
          canvas, trackedPos.left + cornerSize, trackedPos.top, labelString + "%", boxPaint);
    }
  }

  private void processResults(final List<Recognition> results) {
    final List<Pair<Float, Recognition>> rectsToTrack = new LinkedList<Pair<Float, Recognition>>();

    screenRects.clear();
    final Matrix rgbFrameToScreen = new Matrix(getFrameToCanvasMatrix());

    for (final Recognition result : results) {
      if (result.getLocation() == null) {
        continue;
      }
      final RectF detectionFrameRect = new RectF(result.getLocation());

      final RectF detectionScreenRect = new RectF();
      rgbFrameToScreen.mapRect(detectionScreenRect, detectionFrameRect);

      logger.v(
          "Result! Frame: " + result.getLocation() + " mapped to screen:" + detectionScreenRect);

      screenRects.add(new Pair<Float, RectF>(result.getConfidence(), detectionScreenRect));

      if (detectionFrameRect.width() < MIN_SIZE || detectionFrameRect.height() < MIN_SIZE) {
        logger.w("Degenerate rectangle! " + detectionFrameRect);
        continue;
      }

      rectsToTrack.add(new Pair<Float, Recognition>(result.getConfidence(), result));
    }

    trackedObjects.clear();
    if (rectsToTrack.isEmpty()) {
      logger.v("Nothing to track, aborting.");
      return;
    }

    for (final Pair<Float, Recognition> potential : rectsToTrack) {
      final TrackedRecognition trackedRecognition = new TrackedRecognition();
      trackedRecognition.detectionConfidence = potential.first;
      trackedRecognition.location = new RectF(potential.second.getLocation());
      trackedRecognition.title = potential.second.getTitle();

      // r&d: todo toinject voicecode
      // todo 0: add active/inactive voicebutton
      // todo 1: read objtitle of centerboxobj only

      // debug: Log.d("---ok 1----", "title = " + potential.second.getTitle());
      callObjName(potential.second.getTitle());

      // todo 2: run titlevoice for centerboxobj
      // todo 3: obstacle - move camera & execute todo 2

      trackedRecognition.color = COLORS[trackedObjects.size()];
      trackedObjects.add(trackedRecognition);

      if (trackedObjects.size() >= COLORS.length) {
        break;
      }
    }
  }

  private void callObjName(String title) {
    if (title.equals("tv")) {
      Log.d("---ok 1----", "title = " + title);

      try {
        if (mp.isPlaying()) {
          mp.stop();
          mp.release();
          mp = MediaPlayer.create(MainActivity.context, R.raw.tv);
        } mp.start();
      } catch(Exception e) { e.printStackTrace(); }
    } else if (title.equals("person")) {
      try {
        if (mp.isPlaying()) {
          mp.stop();
          mp.release();
          mp = MediaPlayer.create(MainActivity.context, R.raw.person);
        } mp.start();
      } catch(Exception e) { e.printStackTrace(); }
    }  else if (title.equals("cat")) {
      try {
        if (mp.isPlaying()) {
          mp.stop();
          mp.release();
          mp = MediaPlayer.create(MainActivity.context, R.raw.cat);
        } mp.start();
      } catch(Exception e) { e.printStackTrace(); }
    } else if (title.equals("umbrella")) {
      try {
        if (mp.isPlaying()) {
          mp.stop();
          mp.release();
          mp = MediaPlayer.create(MainActivity.context, R.raw.umbrella);
        } mp.start();
      } catch(Exception e) { e.printStackTrace(); }
    } else if (title.equals("bicycle")) {
      try {
        if (mp.isPlaying()) {
          mp.stop();
          mp.release();
          mp = MediaPlayer.create(MainActivity.context, R.raw.bicycle);
        } mp.start();
      } catch(Exception e) { e.printStackTrace(); }
    } else if (title.equals("motorcycle")) {
      try {
        if (mp.isPlaying()) {
          mp.stop();
          mp.release();
          mp = MediaPlayer.create(MainActivity.context, R.raw.motorcycle);
        } mp.start();
      } catch(Exception e) { e.printStackTrace(); }
    } else if (title.equals("toilet")) {
      try {
        if (mp.isPlaying()) {
          mp.stop();
          mp.release();
          mp = MediaPlayer.create(MainActivity.context, R.raw.toilet);
        } mp.start();
      } catch(Exception e) { e.printStackTrace(); }
    } else if (title.equals("bird")) {
      try {
        if (mp.isPlaying()) {
          mp.stop();
          mp.release();
          mp = MediaPlayer.create(MainActivity.context, R.raw.bird);
        } mp.start();
      } catch(Exception e) { e.printStackTrace(); }
    } else if (title.equals("car")) {
      try {
        if (mp.isPlaying()) {
          mp.stop();
          mp.release();
          mp = MediaPlayer.create(MainActivity.context, R.raw.person);
        } mp.start();
      } catch(Exception e) { e.printStackTrace(); }
    } else if (title.equals("spoon")) {
      try {
        if (mp.isPlaying()) {
          mp.stop();
          mp.release();
          mp = MediaPlayer.create(MainActivity.context, R.raw.person);
        } mp.start();
      } catch(Exception e) { e.printStackTrace(); }
    } else if (title.equals("traffic light")) {
      try {
        if (mp.isPlaying()) {
          mp.stop();
          mp.release();
          mp = MediaPlayer.create(MainActivity.context, R.raw.trafficlight);
        } mp.start();
      } catch(Exception e) { e.printStackTrace(); }
    } else if (title.equals("hair drier")) {
      try {
        if (mp.isPlaying()) {
          mp.stop();
          mp.release();
          mp = MediaPlayer.create(MainActivity.context, R.raw.person);
        } mp.start();
      } catch(Exception e) { e.printStackTrace(); }
    } else if (title.equals("scissors")) {
      try {
        if (mp.isPlaying()) {
          mp.stop();
          mp.release();
          mp = MediaPlayer.create(MainActivity.context, R.raw.person);
        } mp.start();
      } catch(Exception e) { e.printStackTrace(); }
    } else if (title.equals("clock")) {
      try {
        if (mp.isPlaying()) {
          mp.stop();
          mp.release();
          mp = MediaPlayer.create(MainActivity.context, R.raw.clock);
        } mp.start();
      } catch(Exception e) { e.printStackTrace(); }
    } else if (title.equals("book")) {
      try {
        if (mp.isPlaying()) {
          mp.stop();
          mp.release();
          mp = MediaPlayer.create(MainActivity.context, R.raw.book);
        } mp.start();
      } catch(Exception e) { e.printStackTrace(); }
    } else if (title.equals("laptop")) {
      try {
        if (mp.isPlaying()) {
          mp.stop();
          mp.release();
          mp = MediaPlayer.create(MainActivity.context, R.raw.laptop);
        } mp.start();
      } catch(Exception e) { e.printStackTrace(); }
    } else if (title.equals("cell phone")) {
      try {
        if (mp.isPlaying()) {
          mp.stop();
          mp.release();
          mp = MediaPlayer.create(MainActivity.context, R.raw.cellphone);
        } mp.start();
      } catch(Exception e) { e.printStackTrace(); }
    } else if (title.equals("chair")) {
      try {
        if (mp.isPlaying()) {
          mp.stop();
          mp.release();
          mp = MediaPlayer.create(MainActivity.context, R.raw.chair);
        } mp.start();
      } catch(Exception e) { e.printStackTrace(); }
    } else if (title.equals("keyboard")) {
      try {
        if (mp.isPlaying()) {
          mp.stop();
          mp.release();
          mp = MediaPlayer.create(MainActivity.context, R.raw.keyboard);
        } mp.start();
      } catch(Exception e) { e.printStackTrace(); }
    } else if (title.equals("mouse")) {
      try {
        if (mp.isPlaying()) {
          mp.stop();
          mp.release();
          mp = MediaPlayer.create(MainActivity.context, R.raw.mouse);
        } mp.start();
      } catch(Exception e) { e.printStackTrace(); }
    } else if (title.equals("bed")) {
      try {
        if (mp.isPlaying()) {
          mp.stop();
          mp.release();
          mp = MediaPlayer.create(MainActivity.context, R.raw.bed);
        } mp.start();
      } catch(Exception e) { e.printStackTrace(); }
    } else if (title.equals("truck")) {
      try {
        if (mp.isPlaying()) {
          mp.stop();
          mp.release();
          mp = MediaPlayer.create(MainActivity.context, R.raw.truck);
        } mp.start();
      } catch(Exception e) { e.printStackTrace(); }
    } else if (title.equals("train")) {
      try {
        if (mp.isPlaying()) {
          mp.stop();
          mp.release();
          mp = MediaPlayer.create(MainActivity.context, R.raw.train);
        } mp.start();
      } catch(Exception e) { e.printStackTrace(); }
    } else if (title.equals("bus")) {
      try {
        if (mp.isPlaying()) {
          mp.stop();
          mp.release();
          mp = MediaPlayer.create(MainActivity.context, R.raw.bus);
        } mp.start();
      } catch(Exception e) { e.printStackTrace(); }
    } else if (title.equals("pizza")) {
      try {
        if (mp.isPlaying()) {
          mp.stop();
          mp.release();
          mp = MediaPlayer.create(MainActivity.context, R.raw.pizza);
        } mp.start();
      } catch(Exception e) { e.printStackTrace(); }
    } else if (title.equals("orange")) {
      try {
        if (mp.isPlaying()) {
          mp.stop();
          mp.release();
          mp = MediaPlayer.create(MainActivity.context, R.raw.orange);
        } mp.start();
      } catch(Exception e) { e.printStackTrace(); }
    } else if (title.equals("apple")) {
      try {
        if (mp.isPlaying()) {
          mp.stop();
          mp.release();
          mp = MediaPlayer.create(MainActivity.context, R.raw.apple);
        } mp.start();
      } catch(Exception e) { e.printStackTrace(); }
    } else if (title.equals("banana")) {
      try {
        if (mp.isPlaying()) {
          mp.stop();
          mp.release();
          mp = MediaPlayer.create(MainActivity.context, R.raw.banana);
        } mp.start();
      } catch(Exception e) { e.printStackTrace(); }
    } else if (title.equals("cup")) {
      try {
        if (mp.isPlaying()) {
          mp.stop();
          mp.release();
          mp = MediaPlayer.create(MainActivity.context, R.raw.cup);
        } mp.start();
      } catch(Exception e) { e.printStackTrace(); }
    } else if (title.equals("handbag")) {
      try {
        if (mp.isPlaying()) {
          mp.stop();
          mp.release();
          mp = MediaPlayer.create(MainActivity.context, R.raw.handbag);
        } mp.start();
      } catch(Exception e) { e.printStackTrace(); }
    } else if (title.equals("bottle")) {
      try {
        if (mp.isPlaying()) {
          mp.stop();
          mp.release();
          mp = MediaPlayer.create(MainActivity.context, R.raw.bottle);
        } mp.start();
      } catch(Exception e) { e.printStackTrace(); }
    } else if (title.equals("dog")) {
      try {
        if (mp.isPlaying()) {
          mp.stop();
          mp.release();
          mp = MediaPlayer.create(MainActivity.context, R.raw.dog);
        } mp.start();
      } catch(Exception e) { e.printStackTrace(); }
    } else if (title.equals("toothbrush")) {
      try {
        if (mp.isPlaying()) {
          mp.stop();
          mp.release();
          mp = MediaPlayer.create(MainActivity.context, R.raw.toothbrush);
        } mp.start();
      } catch(Exception e) { e.printStackTrace(); }
    }

  }

  private static class TrackedRecognition {
    RectF location;
    float detectionConfidence;
    int color;
    String title;
  }
}
